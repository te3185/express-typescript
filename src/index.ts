import express from 'express';

import { APP_PORT } from './config';
import { GreetingMessage } from './greeting';
import { logger } from './logger';

const app = express();

app.get('/', (req, res) => {
  const greeting = GreetingMessage();
  res.send(greeting);
});

app.listen(APP_PORT, () => {
  logger.info(`Application running on http://localhost:${APP_PORT}`);
  logger.error('Double Quote');
});
